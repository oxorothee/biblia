# libraries
import os
import re
import requests
import discord


from datetime import datetime
from dotenv import load_dotenv

# project
from api import channel as channel_list
from api import database,texte,log,dispatch, embed



load_dotenv()





# charge les emojis utilisés pour interragir
EMOJI_BOOK_ORANGE= "📙"
EMOJI_ONE= "1️⃣"
EMOJI_TWO= "2️⃣"
EMOJI_THREE= "3️⃣"
EMOJI_FOUR= "4️⃣"
EMOJI_FIVE= "5️⃣"
EMOJI_SIX= "6️⃣"
EMOJI_SEVEN= "7️⃣"
EMOJI_EIGHT= "8️⃣"
EMOJI_NINE= "9️⃣"
EMOJI_ZERO= "0️⃣"
EMOJI_V= "✅"
EMOJI_X= "❌"
EMOJI_AUTOR= "👤"
EMOJI_TITLE= "📜"
EMOJI_DATE= "📅"
EMOJI_DESC= "📝"
EMOJI_CATEGORY= "📂"
EMOJI_ITW= "🎬"
EMOJI_COVER= "🖼️"
EMOJI_FILE= "📖"

EMOJI_BOOK_BLUE=os.getenv("EMOJI_BOOK_BLUE")
EMOJI_BOOK_EDIT=os.getenv("EMOJI_BOOK_EDIT")
EMOJI_BOOK_SEARCH=os.getenv("EMOJI_BOOK_SEARCH")

CHANNEL_DUMP=os.getenv("CHANNEL_DUMP")
MIROIR_CHANNEL=int(os.getenv("MIROIR_CHANNEL"))


txt_enregistre = (  "« *Que voulez-vous faire ?* »\n\n\t📘 - **Ajouter** un ouvrage\n\t📝 - **Modifier** un ouvrage\n\t🔍 - **Rechercher** un ouvrage\n",
                    "Quelle information voulez vous modifier à la fiche du livre ?\n\t👤 - Auteur(s)\n\t📜 - Titre\n\t📅 - Année de publication\n\t📝 - Citation ou Résumé\n\t📂 - Catégorie\n\t🎬 - Cité en interview (optionnel)\n\t🖼️ - Image de couverture\n\t📖 - Fichier du livre\n\t✅ - Valider la publication\n\t❌ - Annuler la publication\n👇 Cliquez sur les réactions ci-dessous afin de renseigner leurs champs 👇\n\n",
                    "Renseignez le titre du livre",
                    "Renseignez le ou les auteurs (séparés par une virgule)",
                    "Renseignez l'**année** de publication",
                    "Renseignez la catégorie ou les catéogires (selectionnez l'émoji correspondant)",
                    "Renseignez une description rapide de l'œuvre",
                    "Fournissez une image de la couverture",
                    "Renseignez la personnalité interviewé chez Thinkerview ayant proposé ce livre",
                    "Envoyez moi le fichier .pdf, .epub, .djvu, .mobi, .zip ou .7z"
                 )
txt_recherche = (   "Cliquez sur l'émoticone pour chercher un livre.",
                    "Quelle élément voulez-vous rechercher ?\n\t1-\ttitre\n\t2-\tauteur\n\t3-\tdate\n\t4-\tcategorie\n\t5-\tdescription\n\t6-\tproposé par (quelle itw thinkerview)",
                    "Selectionnez un titre"
                 )




list_emoji_step = {
    EMOJI_BOOK_BLUE: 1,
    EMOJI_AUTOR: 3,
    EMOJI_TITLE: 2,
    EMOJI_DATE: 4,
    EMOJI_DESC: 6,
    EMOJI_CATEGORY: 5,
    EMOJI_ITW: 8,
    EMOJI_COVER: 7,
    EMOJI_FILE: 9,
    EMOJI_V: 0,
    EMOJI_X: 1
    }




"""
Fonctions gérant l'envoie de texte

"""

async def session_print(session, bot):

    """
    affiche la session a l'utilisateur

    :param session: instance de la session de l'utilisateur
    :param bot: instance du bot
    :return:
    """

    # print(session)
    # print(session.channel_id)
    channel = bot.get_channel(session.channel_id)

    # note : there is no simple way to purge DMChannel
    # await channel.purge()

    # Registration has two dialogs : 
    # 1 - asking which data to modify (or add)
    # 2 - asking for the value of it.
    # specific case : input for categories isn't a string but a reaction
    # print(session.book)

    if session.step == 1:
        await channel.send(embed=embed.create_embed(session.book))
        step1_mess = await channel.send(session.message.registration(session, valide=book_complete(session)))
        step1_emoji = session.message.registration(session, valide=book_complete(session), emoji=True)
        for emoji in step1_emoji:
            await step1_mess.add_reaction(emoji)
        return
        
    elif session.step == 5:
        print("step 5 : categorie")
        step5_mess = await channel.send(session.message.registration(session, valide=book_complete(session)))
        step5_emoji = session.message.registration(session, valide=book_complete(session), emoji=True)
        for emoji in step5_emoji:
            await step5_mess.add_reaction(emoji)

        mess_chanlist = "\n"
        for chan in channel_list.list_channels:
            mess_chanlist += str(chan) + "\n"
        mess = await channel.send(mess_chanlist)

        for chan in channel_list.list_channels:
            await mess.add_reaction(chan.name[0])
        return
    
    else:
        message = await channel.send(session.message.registration(session,valide=book_complete(session)))
        for emoji in session.message.registration(session,valide=book_complete(session),emoji=True):
            await message.add_reaction(emoji)
        return

    

"""
 Manage reaction input for recording session

"""

async def session_input_reaction(session, reaction, bot):

    """
    gere les reaction de l'uilistateur pour la session registration ou edit


    :param session: instance de la session d'enregistrement
    :param reaction: objet reaction
    :param bot: instance du bot discord
    :return:
    """

    if str(reaction.emoji) == "⭕" and session.step in {0,1}:
        await reaction.message.channel.send("Session annulée. Fermeture de cette session.")
        await dispatch.close_session(session)
        return

    if str(reaction.emoji) == "❌" and session.step in {2,3,4,5,6,7,8,9}:
        session.step = list_emoji_step[reaction.emoji]
        await session_print(session, bot)
        return
        

    if session.step not in {1,5}:
        return

    if (session.step == 1 and (reaction.emoji not in list_emoji_step or reaction.emoji == EMOJI_BOOK_BLUE)):
        return

    if (session.step == 5 and (reaction.emoji not in channel_list.list_emojis)):
        return


    if session.step == 1:
        if reaction.emoji == "✅":
            if session.type == "edit":
                if book_complete(session):
                    await publish_book(session, bot)
                    database.modifier_un_livre(session.book["fiche_id"],session.book)
                    await reaction.message.channel.send("La fiche du livre a bien été modifiée !")
                    await dispatch.close_session(session)
            else:
                if book_complete(session):
                    print("livre enregistré.")
                    await publish_book(session, bot)
                    await reaction.message.channel.send("Fermeture de cette session.")
                    await dispatch.close_session(session)
                else:
                    await reaction.message.channel.send(log("validation"))
            return

        session.step = list_emoji_step[reaction.emoji]


        await session_print(session, bot)
        return

    elif session.step == 5:
        if session.type == 'edit':
            session.step = 1
            return


        chan_name = str(channel_list.list_channels[channel_list.list_emojis.index(reaction.emoji)])
        session.book["channel_id"] = channel_list.get_channel_id(chan_name)
        session.book["categories"] = chan_name
        
        session.step = list_emoji_step[EMOJI_BOOK_BLUE]
        await session_print(session, bot)
        return




"""
 Manage text input for recording session

"""

async def session_input_message(session, message, bot):

    """
    gere l'entrer de message dans une session registration ou edit

    :param session: instance de la session
    :param message: instance du message
    :param bot: instance du bot
    :return:
    """
            
    # II : étape attendant du texte (2 à N)
    if session.step in {1,5}:
        # await message.delete()
        return

    if session.step == 0: # modification uniquement, sert a recuperer la fiche du livre dans la bibliotheque
        if message.content.startswith("https://discordapp.com/") or message.content.startswith("https://discord.com/"):
            id_livre = message.content.split("/")
            id_livre = int(id_livre[-1])
            add = database.recuperer_un_livre(id_livre)

            if add == None or add == False:
                await message.channel.send("erreur : le lien ne mene pas vers une fiche enregistre dans la bibliotheque \n \n ")
                return

            else:
                session.book = add
                channel = bot.get_channel(int(session.book['channel_id']))
                message = await channel.fetch_message(int(session.book["fiche_id"]))
                session.book["img_cover"] = message.embeds[0].image.url

                session.step = 1
                await session_print(session, bot)
            return

        else:
            await message.channel.send("erreur: ce n'est pas un lien discord")
            return



    
    # sanitize la saisie, on retire ce dont on ne veut pas :
    # sanitize input, remove what we don't want : 
    # return, useless empty spaces start & end of input + arround , or ;
    # exception for the 6th step, the description.
    if session.step != 6:
        saisie = re.sub("\n", "", message.content)
        saisie = re.sub("^[ ]*|[ ]*$", "", saisie)


    if session.step == 2:
        session.book["title"] = saisie.title()

    if session.step == 3:
        # there can be various authors for the same book.


        """saisie = re.sub("[ ]*[,|;][ ]*", ",", message.content)
        authors = re.split(",|;", message.content)
        if "" in authors:
            authors.remove("")
        if " " in authors:
            authors.remove(" ")"""
        session.book["authors"] = message.content.title()

    if session.step == 4:
        # date must be a year [-3000 ; $current_year]
        if saisie.lstrip('-+').isdigit():
            pub_year = int(saisie)
            if pub_year <= datetime.now().year and pub_year >= -3000:
                session.book["date"] = saisie
            else:
                pass
        else:
            pass

    if session.step == 6:
        session.book["description"] = message.content


    # image cover
    if session.step == 7:
        if message.attachments:
            if message.attachments[0].filename.endswith(tuple([".jpg", ".jpeg", ".png", ".gif", ".webp"])):
                img_file = await message.attachments[0].to_file()
                dump_channel = bot.get_channel(int(CHANNEL_DUMP))
                post = await dump_channel.send(file=img_file)
                session.book["img_cover"] = post.attachments[0].url
            else:
                await message.channel.send("Le livre doit être sous format .jpg, .png, .gif ou .webp")
        else:
            regex = re.compile(
                        r'^(?:http|ftp)s?://' # http:// or https://
                        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
                        r'localhost|' #localhost...
                        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
                        r'(?::\d+)?' # optional port
                        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

            if re.match(regex, message.content) is None:
                return

            request = requests.get(message.content)

            if request.status_code == 200:
                session.book["img_cover"] = message.content


    if session.step == 8:
        session.book["recommended_by"] = message.content.title()


    if session.step == 9:
        if session.type == 'type':
            return
        if message.attachments:
            if message.attachments[0].filename.endswith(tuple([".pdf", ".epub", ".mobi", ".djvu", ".zip", ".7z"])):
                session.book["file"] = await message.attachments[0].to_file()
            else:
                await message.channel.send("Le livre doit être sous format .pdf, .epub, .djvu, .mobi, .zip ou .7z")
        else:
            pass


    # après une saisie de donnée, on revient toujours à l'étape 1 (demande quelle info)
    # on ignore donc les étapes 0 et 1. L'étape validation est impossible
    session.step = 1


    await session_print(session, bot)
    return





async def publish_book(session, bot):

    """
    publie la fiche du livre dans le bon salon

    :param session: instance de la session
    :param bot: instance du bot
    :return:
    """

    if session.type == 'registration':

        # publication
        channel = bot.get_channel(session.book["channel_id"])
        sent_message = await channel.send(embed = embed.create_embed(session.book), file=session.book["file"])
        await sent_message.add_reaction("📖")
        await sent_message.add_reaction("❤️")
        await sent_message.add_reaction("👎")
        session.book["url"] = sent_message.jump_url
        session.book["fiche_id"] = sent_message.id

        # publication miroir
        channel = bot.get_channel(MIROIR_CHANNEL)
        miroir = await channel.send(embed = embed.create_embed(session.book), file=session.book["file"])
        session.book["miroir_id"] = miroir.id

        database.ajouter_un_livres(session.book)
    elif session.type == 'edit':

        channel = bot.get_channel(int(session.book['channel_id']))
        message = await channel.fetch_message(int(session.book["fiche_id"]))
        await message.edit(embed = embed.create_embed(session.book))

        channel = bot.get_channel(MIROIR_CHANNEL)
        message = await channel.fetch_message(int(session.book["miroir_id"]))
        await message.edit(embed=embed.create_embed(session.book))
    return 


def book_complete(session):

    """
    controle que tout les element obligatoire de la fiche du livre soit bien remplie

    :param session: instance de la session
    :return: bool, true, la fiche est est complete
    """

    if session.type == 'edit':

        if (session.book["title"]
                and session.book["authors"]
                and session.book["date"]
                and session.book["description"]
                and session.book["categories"]
                and session.book["img_cover"]):
            return True
        return False


    elif (session.book["title"]
            and session.book["authors"]
            and session.book["date"]
            and session.book["description"]
            and session.book["categories"]
            and session.book["img_cover"]
            and session.book["file"]):
        return True
    return False

