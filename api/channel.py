import os


from dotenv import load_dotenv
load_dotenv()


GUILD_LIBRARY=os.getenv("GUILD_LIBRARY")
CATEGORY_LIBRARY=os.getenv("CATEGORY_LIBRARY")


list_channels = []
list_emojis = []


async def load_channels(bot):
    global list_channels
    global list_emojis

    guild = bot.get_guild(int(GUILD_LIBRARY))
    for cat in guild.categories:
        if int(cat.id) == int(CATEGORY_LIBRARY):
            list_channels = cat.channels.copy()
            break

    for chan in list_channels:
        print("liste channel = " + chan.name)
        list_emojis.append(chan.name[0])





async def list_all(channel):
    txt=f"Liste de toutes les catégories de la bibliothèque :\n"
    for item in list_channels:
        txt = txt + f"\n\t- {item.name}"
    await channel.send(f"{txt}")


def get_channel_id(name):
    for item in list_channels:
        if item.name == name:
            return item.id

def channel_list_has(channel_id):
    for item in list_channels:
        if channel_id == item.id:
            return True
    return False
