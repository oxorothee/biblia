# libraries
import os
from dotenv import load_dotenv

# project
from api import embed,database


load_dotenv()
ENTRY_CHANNEL = int(os.getenv("ENTRY_CHANNEL"))


class Home:

    async def print(self,bot):

        """
        affiche le message d'entrer (commande)

        :param bot: instance du bot
        :return:
        """
        channel = bot.get_channel(ENTRY_CHANNEL)

        await channel.purge()
        self.home = await channel.send(embed = embed.embed_start(database.biblia_compteur()))
        await self.home.add_reaction("📘")
        await self.home.add_reaction("📝")
        # await self.home.add_reaction("🔍")

        # aide du bot
        #await self.home.add_reaction("❔")


    async def edit(self):
        """
        actualise le message d'entréé

        :return:
        """
        await self.home.edit(embed = embed.embed_start(database.biblia_compteur()))
