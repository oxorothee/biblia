from api.objet.message import Message


class Session:
    def __init__(self):
        self.message = Message()
        self.type = ""
        self.step = 1
        self.channel_id = ""
        self.private = True
        self.user_id = ""
        self.user_name = ""
        self.admin = False







class Session_search(Session):

    def __init__(self):
        super().__init__()
        self.type = "search"


        self.fiche = {
            "user_id": "",
            "mot_clef": "",
            "categories": [],
            "siecle": "",
            "tkw": "",
            "readlist": "",
            "coupdecoeur": ""
        }

    def __str__(self):
        return f"<session : {self.user_id} / user name : {self.user_name} / session type : {self.type} / admin : {self.admin} / session date open : pas disponible /" \
               f" session step : {self.step} / {self.fiche}>"






class Session_registration(Session):
    def __init__(self,edit = False):
        super().__init__()
        self.type = "registration"

        self.book = {
            "title": "",
            "authors": "",
            "date": "",
            "categories": "",
            "channel_id": "",
            "description": "",
            "recommended_by": "",
            "url" : "",
            "fiche_id" : "",
            "miroir_id" : "",
            "img_cover": "",
            "file": ""}



    def __str__(self):
        return f"<session : {self.user_id} / user name : {self.user_name} / session type : {self.type} / admin : {self.admin} / session date open : pas disponible /" \
               f" session step : {self.step} / {self.book}>"


class Session_edit(Session):
    def __init__(self):
        super().__init__()
        self.step = 0
        self.type = "edit"

        self.book = {
            "title": "",
            "authors": "",
            "date": "",
            "categories": "",
            "channel_id": "",
            "description": "",
            "recommended_by": "",
            "url": "",
            "fiche_id": "",
            "miroir_id": "",
            "img_cover": "",
            "file": ""}

    def __str__(self):
        return f"<session : {self.user_id} / user name : {self.user_name} / session type : {self.type} / admin : {self.admin} / session date open : pas disponible /" \
               f" session step : {self.step} / {self.book}>"











