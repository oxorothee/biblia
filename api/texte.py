langage_list = [["fr",""],["en",""]]


texte = {
    # message home
    1: '« *Que voulez-vous faire ?* »\n\n\t📘 - **Ajouter** un ouvrage\n\t📝 - **Modifier** un ouvrage\n\t🔍 - **Rechercher** un ouvrage',

    # --------------------------------------------------------- search -------------------------------------------------------------------
    "search": {
            # step 1
        "step1/1":{"fr":"*** session recherche ***",
            "en":""},

        "step1/2":{"fr":"🔎 lancer la recherche",
            "en":""},

        "step1/3":{"fr":"⭕️fermer la session de recherche",
            "en":""},

        "step1/4":{"fr":"🔑 ajouter un mots clef à la recherche",
            "en":""},

        "step1/5":{"fr":"🔑 modifier : le mots clef à la recherche, actuelle = ",
            "en":""},

        "step1/6":{"fr":"📂 sélectionner les catégories dans lesquelles effectuer la recherche",
            "en":""},

        "step1/7":{"fr":"📂 modifier : les catégories dans lesquelles effectuer la recherche actuelle = ",
            "en":""},

        "step1/8":{"fr":"📅 sélectionner un siècle dans lequel effectuer la recherche",
            "en":""},

        "step1/9":{"fr":"📅 modifier : le siècle dans lequel effectuer la recherche actuelle = ",
            "en":""},

        "step1/10":{"fr":"🎬 sélectionner les ouvrages proposés part Thinkerview",
            "en":""},

        "step1/11":{"fr":"🎬 modifier : actuelle = seulement les ouvrage proposés part la communauté",
            "en":""},

        "step1/12":{"fr":"🎬 modifier : actuelle = seulement les ouvrages cités dans Thinkerview",
            "en":""},

        "step1/13":{"fr":"📖 mes livres déjà lus",
            "en":""},

        "step1/14":{"fr":"📖 modifier : actuelle = seulement les ouvrage que j'ai lu",
            "en":""},

        "step1/15":{"fr":"📖 modifier : actuelle = seulement les ouvrages que je n'ai pas lu",
            "en":""},

        "step1/16":{"fr":"❤️ mes coups de cœur",
            "en":""},

        "step1/17":{"fr":"❤️ modifier : actuelle = seulement mes coups de cœur",
            "en":""},

        "step1/18":{"fr":"❤️ modifier : actuelle = seulement les ouvrages qui ne sont pas dans mes coups de cœur",
            "en":""},

                        #step 2
        "step2/1":{"fr":"entrez les mots clefs que vous souhaitez rechercher\n❌ retour au menu",
            "en":""},

                        #step 3
        "step3/1":{"fr":"sélectionnez la catégorie que vous souhaitez rechercher",
            "en":""},


                        #step 3
        "step4/1":{"fr":"entrez un siècle (exemple, 17ème siècle: 17)\n❌ retour au menu",
            "en":""},

                        #step 5
        "step5/1":{"fr":"✅ uniquement les livres recommandés en interview\n⏹ uniquement les livres proposés par la communauté\n❌ retour au menu",
            "en":""},

                        #step 6
        "step6/1":{"fr":"✅ uniquement les livres que j'ai lu\n⏹ uniquement les livres que je n'ai pas lu\n❌ retour au menu",
            "en":""},

                        #step 7
        "step7/1":{"fr":"✅ uniquement les livres dans mes coups de cœur\n⏹ uniquement les livres qui ne sont pas dans mes coups de cœur\n❌ retour au menu",
            "en":""},
        },

    "registration":{
        "step0/1":{ "fr": "\nentrez l'url du post que vous souhaitez modifier ou annuler\n\n 1) clic droit sur un post biblia d'une fiche de livre\n 2) copiez le lien du message\n\n⭕ fermer la session",
                    "en": ""},

        "step1/1":{ "fr": "\n***Quelle information voulez-vous modifier à la fiche du livre ?***",
                    "en": ""},

        "step1/2":{ "fr": "\n\n***Quelle information voulez-vous ajouter à la fiche du livre ?***",
                    "en": ""},

        "step1/3":{ "fr": "\n\t***Information déjà rentrée, vous pouvez les modifier***",
                    "en": ""},

        "step1/4":{ "fr": "\n\t👤 - Auteur(s)",
                    "en": ""},

        "step1/5": {"fr": "\n\t📅 - Année de publication",
                    "en": ""},

        "step1/6": {"fr": "\n\t📝 - Citation ou Résumé",
                    "en": ""},

        "step1/7": {"fr": "\n\t🎬 - Cité en interview (optionnel)",
                    "en": ""},

        "step1/8": {"fr": "\n\t🖼️ - Image de couverture",
                    "en": ""},

        "step1/9": {"fr": "\n\t✅ - Valider la publication",
                    "en": ""},

        "step1/10": {"fr": "\n\t⭕ - Annuler la publication",
                    "en": ""},

        "step1/11": {"fr": "\n\t❌ - Revenir au menu",
                    "en": ""},

        "step1/12": {"fr": "\n\t🗑 - Supprimer le livre",
                    "en": ""},

        "step1/13": {"fr": "\n👇 Cliquez sur les réactions ci-dessous afin de renseigner leurs champs 👇",
                    "en": ""},

        "step1/14": {"fr": "\n***Informations à compléter pour faire la modification***",
                    "en": ""},

        "step1/15": {"fr": "\n\t📂 - Catégorie",
                    "en": ""},

        "step1/16": {"fr": "\n\t📖 - Fichier du livre",
                    "en": ""},

        "step1/17": {"fr": "\n\t📜 - Titre",
                    "en": ""},

        "step2/1": {"fr": "Modifier le titre du livre",
                    "en": ""},

        "step2/2": {"fr": "Renseignez le titre du livre",
                    "en": ""},

        "step3/1": {"fr": "Modifier le ou les auteurs (séparés par une virgule)",
                    "en": ""},

        "step3/2": {"fr": "Renseignez le ou les auteurs (séparés par une virgule)",
                    "en": ""},

        "step4/1": {"fr": "Modifier l'**année** de publication",
                    "en": ""},

        "step4/2": {"fr": "Renseignez l'**année** de publication",
                    "en": ""},

        "step5/1": {"fr": "Modifier la catégorie (sélectionnez l'émoji correspondant)",
                    "en": ""},

        "step5/2": {"fr": "Renseignez la catégorie (sélectionnez l'émoji correspondant)",
                    "en": ""},

        "step6/1": {"fr": "Modifier la description de l'œuvre",
                    "en": ""},

        "step6/2": {"fr": "Renseignez une description rapide de l'œuvre",
                    "en": ""},

        "step7/1": {"fr": "Modifier l'image de couverture",
                    "en": ""},

        "step7/2": {"fr": "Fournissez une image de couverture",
                    "en": ""},

        "step8/1": {"fr": "Modifier la personnalité interviewée chez Thinkerview ayant proposée ce livre",
                    "en": ""},

        "step8/2": {"fr": "Renseignez la personnalité interviewée chez Thinkerview ayant proposée ce livre",
                    "en": ""},

        "step9/1": {"fr": "Envoyez moi le fichier .pdf ou .epub du livre",
                    "en": ""},

        "step9/2": {"fr": "Envoyez moi le fichier .pdf ou .epub du livre",
                    "en": ""},

        "step10/1": {"fr": "La fiche de l'ouvrage a été modifiée avec succès, merci pour votre contribution",
                    "en": ""},

        "step10/2": {"fr": "L'ouvrage a bien été partagé, de la part de toute la communauté, merci pour votre contribution",
                    "en": ""}
        },

    "welcome":{
        1 : {"fr":"Bienvenu dans La Biliothèque *",
            "en":""},
        2 : {"fr":"*,"
                  "\n"
                  "\nJe suis Biblia, la bibliothécaire qui gère ce lieu, j'aide les membres à partager et chercher des livres."
                  "\nPour me demander quelque chose, il te suffit d'aller dans **accueil -> biblia**, tu y trouveras aussi l'aide sur mon fonctionnement"
                  "\n"
                  "\nps : tu trouveras 3 icônes sous la fiche de chaque ouvrage"
                  "\n 📖 pour enregistrer le livre dans ta liste des livre lus"
                  "\n ❤ pour enregistrer le livre dans ta liste des coups de cœur"
                  "\n 👎 pour dire que tu n’as pas aimé ce livre",
            "en":""}},

    "help":{
        "sommaire":
            {"fr":"-                           ***aide de Biblia***"
                  "\n"
                  "\n **sommaire**"
                  "\n1) agencement de la bibliotheque"
                  "\n2) presentation de la fiche d'une livre"
                  "\n3) ajouter un livre"
                  "\n4) modifier un livre"
                  "\n5) rechercher un livre"
                  "\n6) utiliser la readlist(liste des livres lu"
                  "\n7) utiliser les coup de coeur(livre apprecier)",
            "en":""},

        "agencement":
            {"fr":"**1)           agencement de la bibliotheque**",
            "en":""},

        "fiche":
            {"fr":"**2)           presentation de la fiche d'une livre**",
            "en":""},

        "ajout":
            {"fr":"**3)           ajouter un livre**",
            "en":""},

        "modification":
            {"fr":"**4)           modifier un livre**",
            "en":""},

        "recherche":
            {"fr":"**5)           rechercher un livre**",
            "en":""},

        "readlist":
            {"fr":"**6)           utiliser la readlist(liste des livres lu)**",
            "en":""},

        "coupdecoeur":
            {"fr":"**7)           utiliser les coup de coeur(livre apprecier)**",
            "en":""},

    },

    27:{"fr":"",
        "en":""},

    28:{"fr":"",
        "en":""},

    29:{"fr":"",
        "en":""},

    30:{"fr":"",
        "en":""},

    31:{"fr":"",
        "en":""},

    32:{"fr":"",
        "en":""},

    33:{"fr":"",
        "en":""},

    34:{"fr":"",
        "en":""},

    35:{"fr":"",
        "en":""},

    36:{"fr":"",
        "en":""},

    37:{"fr":"",
        "en":""},

    38:{"fr":"",
        "en":""},

    39:{"fr":"",
        "en":""},

    40:{"fr":"",
        "en":""}

}
