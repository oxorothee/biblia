# libraries


# project
from api.log import date
from api.objet.session import Session_registration,Session_search, Session_edit
from api.objet.home import Home
from api import search,registration


list_session = {}
home = Home()

"""
creation d'une session recherche, edition, ou enregistrement,
un utilisitateur ne peut pas avoir deux session ouverte en meme temps

:param bot: instance du bot discord
:param reaction: reaction au message d'entrer du bot
:param user: istance de l'utilisateur qui a lancer une session
:return:
"""
async def create_session(bot, reaction, user):


    # check input is correct
    if(str(reaction.emoji) not in ["📘","📝","🔍","❔"]):
        await reaction.remove(user)
        return

    if str(reaction.emoji) == "❔":
        # affiche l'aide du bot
        return

    # Manage if user has already an open session 
    if user.id in list_session:
        await reaction.remove(user)
        await user.send("Vous avez déjà une session ouverte")
        return



    channel = await user.create_dm()

    # lance une session d'enregistrement
    if str(reaction.emoji) == "📘":
        new_sess = Session_registration()
        new_sess.channel_id = channel.id
        new_sess.user_id = user.id
        new_sess.user_name = user.name
        new_sess.admin = check_droit_admin(user)
        await registration.session_print(new_sess, bot)


    # lance une session d'edition
    elif str(reaction.emoji) == "📝":
        new_sess = Session_edit()
        new_sess.channel_id = channel.id
        new_sess.user_id = user.id
        new_sess.user_name = user.name
        new_sess.admin = check_droit_admin(user)
        await registration.session_print(new_sess, bot)

    # lance une session de recherche
    elif str(reaction.emoji) == "🔍":
        print("not possible for now");
        """
        new_sess = Session_search()
        new_sess.channel_id = channel.id
        new_sess.user_id = user.id
        new_sess.fiche['user_id'] = user.id
        new_sess.user_name = user.name
        new_sess.admin = check_droit_admin(user)
        await search.session_print(new_sess, bot)
        """
        


    # ajoute la nouvelle session a la liste des session ative
    print(f"ouverture d'une session : {new_sess}")
    list_session[user.id] = new_sess



    # supprime la reaction du poste de commande biblia
    await reaction.remove(user)



async def close_session(session ,byuser=False):

    """
    ferme la session d'un utlisateur

    :param session: instance de la session a fermer
    :param byuser: e sert plus a priorie
    :return:
    """
    if byuser:
        if session not in list_session:
            return
        list_session.pop(session)


    else:
        if session.user_id not in list_session:
            return
        print(f"{date()} / close session / {session.user_id}")
        list_session.pop(session.user_id)

    # remet a jours les statistique
    await home.edit()




async def dispatch_reaction_input(bot, reaction, user):

    """
    disperese les reaction  dans les bonne procedure

    :param bot: instance du bot discord
    :param reaction: reaction a disperser
    :param user: instance utilisateur autheur de la reaction
    :return:
    """

    # control que l'utilistaeur a bien une session active
    if user.id not in list_session:
        return

    session = list_session[user.id]

    if reaction.message.channel.id != session.channel_id:
        return

    if list_session[user.id].type == "registration":
        await registration.session_input_reaction(session, reaction, bot)
    elif list_session[user.id].type == "edit":
        await registration.session_input_reaction(session, reaction, bot)
    elif list_session[user.id].type == "search":
        await search.session_input_reaction(session, bot, reaction)

    print(f"{date()} / session = {session}")


async def dispatch_message_input(bot, message):

    """
    disperse les message dans la bonne procedure

    :param bot: instance du bot dicord
    :param message: instance du message
    :return:
    """

    # controle que l'utilistaeur a bien une session d'ouverte
    if message.author.id not in list_session:
        return

    session = list_session[message.author.id]

    if message.channel.id != session.channel_id:
        return

    if list_session[message.author.id].type == "registration":
        await registration.session_input_message(session, message, bot)
    elif list_session[message.author.id].type == "edit":
        await registration.session_input_message(session, message, bot)
    elif list_session[message.author.id].type == "search":
        await search.session_input_message(session, bot, message)

    print(f"{date()} / session = {session}")


def check_droit_admin(user):
    """
    controlle que l'utilisteur a bien les droit admin sur le serveur

    :param user: instance de l'utilisateur
    :return: bool, true si admin, false sinon
    """
    return user.guild_permissions.administrator
