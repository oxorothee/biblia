# libraries
import discord
import os

from discord.ext import commands
from dotenv import load_dotenv


# project
from api import dispatch as session_manager
from api import channel as channel_list
from api import database
from api.objet.message import bot_message



# define  variables for the project 

load_dotenv()

token=os.getenv("TOKEN")
ENTRY_CHANNEL = int(os.getenv("ENTRY_CHANNEL"))



# intents = discord.Intents.default()
intents = discord.Intents.all()
print(intents)


bot = commands.Bot(command_prefix=".", intents=intents)


"""
 Fonctions in the main.py manage the signals emitted by the client

"""


"""
on_ready()
Display bot info, initiate data_base and sessions to print the main post
"""
@bot.event
async def on_ready():
    print("Babelia est connecté avec le nom : " + bot.user.name)
    print("Identifiant du bot : " + str(bot.user.id))
    print("---------")
    await session_manager.home.print(bot)
    database.creat_database()
    #session_manager.init_sessions()
    # await session_manager.print_sessions(bot)

    await channel_list.load_channels(bot)




"""
on_member_join(member)
Send a Direct message to a member to welcome him. 

nb : further discussion needed about the usefulness of this feature
"""
@bot.event
async def on_member_join(member):

    channel = await member.create_dm()
    await channel.send(bot_message.welcome(member.name))



"""
on_message(message)
Manage received messages by users. 

"""
@bot.event
async def on_message(message):

    # print(message.attachments)


    if message.author.id == bot.user.id:
        return


    # todo : print babelia's help, check user's right to do so.
    if message.channel.id == 753281814713335808:
        if message.content == '!babelia':
            print("Fonctionnalité non développée : Affichage des commandes possilbes.")

        # list all channels used by the bot
        elif message.content == "!babelia list":
            await channel_list.list_all(message.channel)

        """elif message.content.startswith("!biblia purge"):
            await message.channel.purge()"""


    # all messages on the ENTRY_CHANNEL are deleted, only reactions are permitted
    # on other channels, manage input
    # TODO : vérifier si lorsque le bot demande une saisie de texte 
    #        on doit bien le faire depuis le même channel ou si on peut le faire ailleurs
    elif message.channel.id == ENTRY_CHANNEL:
        await message.delete()
    elif str(message.channel.type) == "private":
        # print(f"user {message.author.name} discute en message privé")
        await session_manager.dispatch_message_input(bot, message)





"""
on_reaction_add(reaction, user)
Manage reaction from users 

"""
@bot.event
async def on_reaction_add(reaction, user):
    if user.id == bot.user.id:
        return
    

    # reactions on ENTRY_CHANNEL are to create a new session (add, search, modify)
    # reaction in other channels are to be handled in the session
    # TODO : check if reaction on any channel impact the current session
    if reaction.message.channel.id == ENTRY_CHANNEL:
        await session_manager.create_session(bot, reaction, user)
    elif str(reaction.message.channel.type) == "private":
        await session_manager.dispatch_reaction_input(bot, reaction, user)
    # else:
        # print("3")






"""
on_raw_reaction_add(ctx)

This function could be (should be?) merged with on_reaction_add(...) 
"""
@bot.event
async def on_raw_reaction_add(ctx):

    if bot.user.id == ctx.user_id:
        return

    if channel_list.channel_list_has(ctx.channel_id):
        if str(ctx.emoji.name) == "📖" :
            database.ajouter_une_lecture(ctx.message_id,ctx.user_id)
        elif str(ctx.emoji.name) == "❤️":
            database.ajouter_un_like(ctx.message_id,ctx.user_id)



"""
on_raw_reaction_remove(ctx):
ce declenche lorsqu'une reaction est ôtée d'un message

Gère la suppression d'un livre de ses favoris ou de ses lectures
"""
@bot.event
async def on_raw_reaction_remove(ctx):


    if bot.user.id == ctx.user_id:
        return

    if channel_list.channel_list_has(ctx.channel_id):
        if str(ctx.emoji.name) == "📖":
            database.suprimer_une_leture(ctx.message_id,ctx.user_id)
        elif str(ctx.emoji.name) == "❤️":
            database.supprimer_un_like(ctx.message_id,ctx.user_id)





bot.run(token)

